#!/usr/bin/perl -w

package csvbankin2;


use YAML::Syck;
use YAML::AppConfig;


#variable declarations
our (
    $conf
    );

#read in configuration
$conf = &read_configuration('config.yaml');
my $format = $conf->get_formats->{asn}->{deb_cred_flag}->{debit};
print "$format\n";
# ====================== #
 # SUBROUTINE DEFINITIONS #
# ====================== #



# =======================
# Sub: read configuration
#

# reads a configuration file and returns a reference to the parsed object?
    
sub read_configuration {
    # step 1: open file TODO use subroutine for this
    open my $fh, '<', $_[0] 
      or die "can't open config file: $!";
    my $string;

    # step 2: slurp file contents
    {
        local $/ = undef;
        binmode $fh;
        $string = <$fh>;
        close $fh;
    }

    # step 3: Load the YAML::AppConfig from the given YAML.
    my $conf = YAML::AppConfig->new(string => $string);
    return $conf;
}


my @bluh = split /,/, $conf->get_formats->{ing}->{fields};

foreach (@bluh){
    print "$_\n";
}


1;
