#!/usr/bin/perl -w


#try with YAML::AppConfig
use YAML::Syck;
use YAML::AppConfig;

# step 1: open file
open my $fh, '<', 'config.yaml' 
  or die "can't open config file: $!";

my $string;
# step 2: slurp file contents
{
    local $/ = undef;
    binmode $fh;
    $string = <$fh>;
    close $fh;
}

# Load the YAML::AppConfig from the given YAML.
my $conf = YAML::AppConfig->new(string => $string);
my $qux = $conf->dump();

#print $qux;

my $qix = $conf->get_accounts->{'em-checking'}->{'format'};
my $qex = $conf->get_formats->{$qix};

print "$qix\n";
my @qix_split = split /,/, $qex;
my $index=1;
foreach (@qix_split){
    print "$index ==> $_\n";
    $index +=1;
}


#try with YAML:XSL

#use Data::Dumper;
#use YAML::XS qw/LoadFile/;

#my $string = <<'YAML';

#    ---
#    foo: world
#    bar: hello
#    baz:
#        - $foo
#        - {foo: dogs, cats: foo}
#        - $foo bar
#    qux:
#        quack: $baz
#YAML

#my $config = LoadFile('config.yaml');
#print "$config->{baz}->[1]->{cats}\n";

