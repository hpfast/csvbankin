package Config;

use strict;

our (
    %accounts, %account_formats, %format_debit_credit_field, $test, %separator,
    );
    

#account formats: enter account details as account name and a comma-separated list of field names describing all the fields in the input file format. Also enter empty fields, for example as unused_1, unused2.

%account_formats = (

    'ing'   => 'date,name,own_accnt_num,transfer_acct,code,deb_cred,amount,mut_type,desc',
    'rabo'  => 'own_acct_num,currency,rent_date,deb_cred,amount,transfer_acct,transfer_name,booking_date,booking_code,filler1,desc_1,desc_2,desc_3,desc_4,desc_5,desc_6,end_to_end_id,id_trans_acct,mandate_id',
    'ov'    => '',
    'asn'   => 'booking_date,own_acct_name,transfer_acct,transfer_name,adres,postcode,place,currency,saldo_pre,mut_currency,ammount,journal_date,currency_date,intern_code,glob_code,seq_num,kenmerk,desc,statement_num',

);

$test = "blah!!!";

#accounts
#enter in accounts and the name of their format.

%accounts = (
    
    'hans-checking'     => 'ing',
    'em-checking'       => 'rabo',
    'hans-ov'           => 'ov',
    'em-ov'             => 'ov',
    'em-saving'         => 'rabo',
    'shared-checking'   => 'asn',
    'shared-saving'     => 'asn',

);

#format_debit_credit_field: a variable specifying whether the format has a separate field marking debit/credit (true), and what field, or uses a negative sign on the amount field (false). Use the string of the field name 0 for false.

%format_debit_credit_field = (

    'ing'   => 'deb_cred',
    'rabo'  => 'deb_cred',
    'ov'    => 0,
    'asn'   => 0,

);

%separator = (

    'ing'   => ',',
    'rabo'  => ',',
    'ov'    => ';',
    'asn'   => ',',

)
