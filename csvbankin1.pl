#!/usr/bin/perl -w

use strict;
no warnings 'uninitialized';
use lib qw(/home/hans/perl5/lib/perl5); #fix this mistake setting of local lib location in cpan...
use File::Basename;

my ($acct_name, $action, $match_strings, %hash, @input_lines, %parsed_lines_hash, $index, @parsed_lines, %final_hash_account_names,%final_hash_match_strings, $filetype);


#my $current_file_full_path = $ARGV[1];
#my $current_file = basename $current_file_full_path;

if( $ARGV[0] !~ /\A(-w|-r)\Z/){
	die "first argument needs to be action type: -r for read, -w for write";
} elsif ($ARGV[0] eq '-w'){
	  &write;
  } elsif ($ARGV[0] eq '-r'){
	  &read;
}
#filetype test
if( $ARGV[1] !~ /\A(ov|ing|rabo)\Z/){
        die "second argument needs to be the type of input file, one of: (ov, ing, rabo).";
        } elsif ($ARGV[1] eq 'ov'){
                $filetype = 'ov';
                  } elsif ($ARGV[1] eq 'ing'){
                            $filetype = 'ing';
                                    } elsif ($ARGV[1] eq 'rabo'){
                                                    $filetype = 'rabo';
                                                          }

#SUBROUTINES
#two subroutines: read and write.
#read reads an input file, parses, matches accounts by match strings, and writes an intermediate file for manual checking/editing.
#write reads the intermediate file, adds new match strings and accounts to the master list, and writes the final parsed output csv with the account name added.
sub read {
	open INPUT_FH, '<', 'output_account_list.txt' or die 'could not open list of accounts/match strings!';
	while (<INPUT_FH>){
		chomp;
		($acct_name, $match_strings) = split /\|/;
		$hash{$acct_name} = $match_strings;
	#	print "$acct_name => $match_strings\n";
	}

	#print "\n\n\n now printing from the hash:\n\n\n";

	#while(($key, $value) = each %hash){
	#	print "$key => $value\n";
	#}

	close INPUT_FH;
	if ($ARGV[0] = '-r'){
		if ($ARGV[2]){
			open INPUT_FH, '<', $ARGV[2]
			or die "could not open input file!";
		} else {
			die 'missing filename to read from';
		}
	}
	@input_lines = <INPUT_FH>; 

	close INPUT_FH;

	$index = 0;

	foreach (@input_lines) {
		s/"//g;
		my ($account_no, $currency, $date, $deb_cred, $amount, $transfer_account, $payee, $date_2, $type, $unknown, $desc1, $desc2, $desc3, $desc4, $desc5, $desc6, $desc7, $desc8, $desc9, ) = split /,/;

		if ($deb_cred eq 'D'){
			$amount = -$amount;
		}


		push(@parsed_lines,"$date;$amount;$transfer_account;$payee;$type;$desc1"); #deprecated
		$parsed_lines_hash{$index}="$date;$amount;$transfer_account;$payee;$type;$desc1";
		$index += 1;
	}
	#foreach $key (keys %parsed_lines_hash){
	#	print "$_\n\n";
	#}
	#reset index
	$index = 0;
	foreach my $parsed_key( keys %parsed_lines_hash){
		my @match_list; #local to the loop over the input line, otherwise it adds up globally
		my $current_input_line = $parsed_lines_hash{$parsed_key};
		foreach my $names_key ( keys %hash){
			my $acct_name = $names_key;
			my @compare_array = split /,/, $hash{$names_key}; #make an array holding the match strings from the current hash entry
	# 		print "$key" if $key =~ /2/;
	#	by comparing to each match string
			foreach (@compare_array){
				my $string = $_;
				if ($current_input_line =~ /\b$string\b/){
					push(@match_list,$string);
					$final_hash_account_names{$parsed_key}=$names_key; #do the account names assignment inside the inner loop because we just want to know once if each account matched (we use the index_lines counter to connect it to the hash strings later)					
				}
			}
			$final_hash_match_strings{$parsed_key}="@match_list" ;
		}

		print "while processing: $current_input_line\nI found the following account: $final_hash_account_names{$parsed_key}\nand the match strings: $final_hash_match_strings{$parsed_key}\n\n";
	#	$index_lines += 1;
	}
	print "-------------------------\n";
	print "FULL ACCOUNT NAMES HASH\n";
	foreach my $k (sort keys %final_hash_account_names){
		print "$k => $final_hash_account_names{$k} => $final_hash_match_strings{$k}\n";
	}

	#trying to print out a list of the fields but currently I have the wrong data.
	print "-------------------------\n";
	print "FULL MATCH STRINGS HASH\n";
	foreach my $j (sort keys %final_hash_match_strings){
		my $field = $final_hash_account_names{$j};
		print "$j => $final_hash_match_strings{$j}=>$field\n";
	}
	open OUTPUT_FH, '>', 'temp_match_file.txt' or die 'could not open interim file!';
	foreach my $i (sort keys %parsed_lines_hash){
		print OUTPUT_FH "$parsed_lines_hash{$i}===>$final_hash_account_names{$i}===>$final_hash_match_strings{$i}\n";
	}
	close OUTPUT_FH;
}

sub write {
	my (%counter,@processed_transaction_lines,$temp_acct_name,%output_accounts,$temp_match_strings,@processed_match_lines);
	#open filehandles for reading in intermediate results and comparing to canonical account list
	open INTERMEDIATE_FH, '<', 'temp_match_file.txt';
	open CANONICAL_LIST, '<', 'testfile.txt';
	chomp (my @intermediate_list = <INTERMEDIATE_FH>);
	chomp (my @canonical_list = <CANONICAL_LIST>);
	close INTERMEDIATE_FH;
	close CANONICAL_LIST;

	foreach my $intermediate_line (@intermediate_list){
		(my $processed_transaction_line, my $temp_acct_name, my $temp_match_strings, my $temp_delete_strings) = split /===>/, $intermediate_line;
    	$counter{$temp_acct_name}=0;
		foreach my $canonical_line (@canonical_list){
			(my $canonical_acct_name, my $canonical_match_strings) = split /\|/, $canonical_line;
		    if ($temp_acct_name eq $canonical_acct_name){
			    if (!$counter{$temp_acct_name}){
				    $counter{$temp_acct_name} += 1;
				    if ($canonical_match_strings !~ /$temp_match_strings/){
				        my $new_canonical_line = $intermediate_line;
				        print "I found one that didn't match! $canonical_line => $new_canonical_line => $counter{$temp_acct_name}\n";
				        $output_accounts{$canonical_acct_name}=$temp_match_strings;
				        next;
				    }else{
				        $output_accounts{$canonical_acct_name}=$temp_match_strings;
			        }
			    }
		    }elsif(! grep /$temp_acct_name/, @canonical_list){
			    if($counter{$temp_acct_name} < 1){
				    $counter{$temp_acct_name} += 1;
				    my $unknown_line = $intermediate_line;
				    print "I found an unknown line! $unknown_line\n";
				    if(! $temp_acct_name == ''){
				        $output_accounts{$temp_acct_name}=$temp_match_strings;
			        }
				    next;
			    }
		    }elsif(! grep /$canonical_acct_name/, @intermediate_list){
			    $output_accounts{$canonical_acct_name}=$canonical_match_strings;
		    }
		#add: delete if matched in third field.	
		}		
		push(@processed_transaction_lines,"$temp_acct_name;$processed_transaction_line");
	}
	open FINAL_OUTPUT_FH, '>>', 'output_file.csv';
	foreach my $line (@processed_transaction_lines){
		print FINAL_OUTPUT_FH "$line\n";
	}
	close FINAL_OUTPUT_FH; 
	open OUTPUT_ACCOUNT_LIST_FH, '>', 'output_account_list.txt';
	foreach my $acct (sort keys %output_accounts){
		print OUTPUT_ACCOUNT_LIST_FH "$acct|$output_accounts{$acct}\n";
	}
	close OUTPUT_ACCOUNT_LIST_FH;
}
