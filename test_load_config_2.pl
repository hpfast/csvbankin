#!/usr/bin/perl -w

use strict;
use YAML::Syck;
use YAML::AppConfig;


my $config_dir = './';
my $config_file = 'csvbankin2.pl';

my $config_file_path = $config_dir . $config_file;
require $config_file_path;

my $format;

my $Config = &csvbankin2::read_configuration('config.yaml');
$format = $Config::formats{ing};

print "$Config\n";
print "$format\n";
