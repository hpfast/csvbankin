#!/usr/bin/perl -w

use strict;

#Now the following produces a match list and can be used to print out the matched accounts. I think it's time to try this out with the real data.
open INPUT_FH, '<', 'testfile.txt' or die "could not open account list!";

while (<INPUT_FH>){
	chomp;
	($acct_name, $match_strings) = split /\|/;
	$hash{$acct_name} = $match_strings;
#	print "$acct_name => $match_strings\n";
}

#print "\n\n\n now printing from the hash:\n\n\n";

#while(($key, $value) = each %hash){
#	print "$key => $value\n";
#}

close INPUT_FH;
open INPUT_FH, '<', 'rabo_test.csv' or die "could not open input file!";
@input_lines = <INPUT_FH>; 

close INPUT_FH;

my %parsed_lines_hash;
my $index = 0;

foreach (@input_lines) {
        s/"//g;
        my ($account_no, $currency, $date, $deb_cred, $amount, $transfer_account, $payee, $date_2, $type, $unknown, $desc1, $desc2, $desc3, $desc4, $desc5, $desc6, $desc7, $desc8, $desc9, ) = split /,/;

        if ($deb_cred eq 'D'){
                $amount = -$amount;
        }


        push(@parsed_lines,"$date;$amount;$transfer_account;$payee;$type;$desc1");
	$parsed_lines_hash{$index}="$date;$amount;$transfer_account;$payee;$type;$desc1";
	$index += 1;
}
#foreach $key (keys %parsed_lines_hash){
#	print "$_\n\n";
#}
#reset index
$index = 0;
foreach $parsed_key( keys %parsed_lines_hash){
	print "!---------------->$parsed_lines_hash{$parsed_key}\n";
	my $acct_name;
	my @match_list; #local to the loop over the input line, otherwise it adds up globally
	$current_input_line = $parsed_lines_hash{$parsed_key};
	foreach $names_key ( keys %hash){
		my $acct_name = $names_key;
 		my @compare_array = split /,/, $hash{$names_key}; #make an array holding the match strings from the current hash entry
# 		print "$key" if $key =~ /2/;
#	by comparing to each match string
		foreach (@compare_array){
			$string = $_;
 			if ($current_input_line =~ /\b$string\b/){
 				push(@match_list,$string);
				$final_hash_account_names{$parsed_key}=$names_key; #do the account names assignment inside the inner loop because we just want to know once if each account matched (we use the index_lines counter to connect it to the hash strings later)		
 				
 			}
#	by comparing directly to the hash keys
#		if ($current_input_line =~ /$hash{$key}/){
#			print "yah!$hash{$key}\n";
#		}
 		}
		$final_hash_match_strings{$parsed_key}="@match_list" ;
 	}
#		print $index_lines;
#	print "\n\n";
	print "while processing: $current_input_line\nI found the following account: $final_hash_account_names{$parsed_key}\nand the match strings: $final_hash_match_strings{$parsed_key}\n\n";
#	$index_lines += 1;
}
print "-------------------------\n";
print "FULL ACCOUNT NAMES HASH\n";
foreach$key (sort keys %final_hash_account_names){
	print "$key => $final_hash_account_names{$key} => $final_hash_match_strings{$key}\n";
}

#trying to print out a list of the fields but currently I have the wrong data.
print "-------------------------\n";
print "FULL MATCH STRINGS HASH\n";
foreach $key (sort keys %final_hash_match_strings){
	$field = $final_hash_account_names{$key};
	print "$key => $final_hash_match_strings{$key}=>$field\n";
}
open OUTPUT_FH, '>', 'temp_match_file.txt' or die 'could not open interim file!';
foreach $key (sort keys %parsed_lines_hash){
	print OUTPUT_FH "$key. $parsed_lines_hash{$key} => $final_hash_account_names{$key} => $final_hash_match_strings{$key}\n";
}
close OUTPUT_FH;
